var React = require('react')
var ReactDOM = require('react-dom')


class MoneyTransferForm extends React.Component {

  constructor(props) {
    super(props);
    this.state = {value: ''};
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({value: event.target.value});
  }

  handleSubmit(event) {

    event.preventDefault();

    if($("input[name='inn_code']")[0].value == "") {
        alert('Не указан ИНН-код счета зачисления средств')
        return
    }

    if($("input[name='money_count']")[0].value == "") {
        alert('Не указана сумма списания денежных средств')
        return
    }

    if(parseInt($("input[name='money_count']")[0].value) < 0.01) {
        alert('Сумма списания не может быть отрицательной')
        return
    }

    if($('select')[0].value == "") {
        alert('Не выбран отправитель')
        return
    }

    $.ajax({
        type: 'POST',
        url: 'api/',
        data: JSON.stringify({
            "from_user": $('select')[0].value,
            "money_count": $("input[name='money_count']")[0].value,
            "to_users": $("input[name='inn_code']")[0].value
        }),
        dataType: "json",
        contentType: "application/json",
        complete: function(data) {
            if(data.status == 500) {
                alert(data.responseText)
            }
        }
    })
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>

        <label>
            Выбрать пользователя
            <select>
                <option value="1">Иван Иванов</option>
                <option value="2">Петр Петров</option>
                <option value="3">Борис	Борисов</option>
                <option value="4">Константин Игорев</option>
                <option value="5">Владимир Игнатьев</option>
                <option value="6">Николай Николаев</option>
                <option value="7">Дмитрий Иванов</option>
                <option value="8">Андрей Константино</option>
                <option value="9">Арсений Антонов</option>
                <option value="10">Павел Георгиев</option>
            </select>
        </label>
        <br></br>

        <label>
            Укажите сумму списания
            <input type="text" name="money_count" value={this.state.value} onChange={this.handleChange} />
        </label>
        <br></br>

        <label>
            Укажите ИНН-счет зачисления
            <input name="inn_code" type="text"/>
            <br></br>(поддерживается ввод через запятую)
        </label>
        <br></br>

        <input type="submit" value="Выполнить перевод" />
      </form>
    );
  }

}

var UsersList = React.createClass({
    loadUsersFromServer: function(){
        $.ajax({
            url: this.props.url,
            datatype: 'json',
            cache: false,
            success: function(data) {
                this.setState({data: data});
            }.bind(this)
        })
    },

    getInitialState: function() {
        return {data: []};
    },

    componentDidMount: function() {
        this.loadUsersFromServer();
        setInterval(this.loadUsersFromServer,
                    this.props.pollInterval)
    }, 
    render: function() {
        if (this.state.data) {
            var userNodes = this.state.data.map(function(user){
                return <tr>
                            <td> {user.user_id} </td>
                            <td> {user.first_name} </td>
                            <td> {user.middle_name} </td>
                            <td> {user.second_name} </td>
                            <td> {user.inn_code} </td>
                            <td> {user.money_balance} </td>
                      </tr>
            })
        }
        return (
            <div>
                <table id="users-list">
                    <thead>
                        <tr>
                            <th>Идентификатор</th>
                            <th>Имя</th>
                            <th>Отчество</th>
                            <th>Фимилия</th>
                            <th>Код ИНН</th>
                            <th>Баланс денежных средств</th>
                        </tr>
                    </thead>
                    <tbody>
                        {userNodes}
                    </tbody>
                </table>
            </div>
        )
    }
})

ReactDOM.render(<MoneyTransferForm/>,
    document.getElementById('transfer_form'))

ReactDOM.render(<UsersList url='/api/' pollInterval={1000} />,
    document.getElementById('container'))


