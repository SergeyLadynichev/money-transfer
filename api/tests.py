# -*- coding: utf-8 -*-
import os
import sys
import django

sys.path.append("/")
os.environ["DJANGO_SETTINGS_MODULE"] = "core.settings"
django.setup()

from django.test import TestCase
from rest_framework.test import APIRequestFactory

from api.models import User
from api.views import UserList


class UsersTestCase(TestCase):

	def setUp(self):
		self.factory = APIRequestFactory()
		self.post_data = {'from_user': 2, 'money_count': 100, "to_users": 552523524}
		self.post_data_negative_payment = {
			'from_user': 2, 'money_count': -100, "to_users": 552523524}

	def test_user_can_pay(self):
		"""
		test user balance. user cant pay over balance. user can`t have negative balance
		"""
		user = User.objects.order_by('?').first()

		self.assertTrue(hasattr(user, 'money_balance'))
		self.assertFalse(user.can_pay(1000000000000000))
		self.assertTrue(user.can_pay(1))
		self.assertTrue(user.money_balance >= 0)

	def test_get_method(self):
		"""
		api testing. checking userList correct working on GET request
		"""
		view = UserList.as_view()
		request = self.factory.get('/')
		response = view(request)

		self.assertEquals(response.status_code, 200)
		self.assertEquals(response.status_text, 'OK')

	def test_post(self):
		"""
		api testing. checking userList correct working on POST request (money transfer logic)
		"""
		request = self.factory.post('/', self.post_data)
		view = UserList.as_view()
		response = view(request)

		self.assertEquals(response.status_code, 200)
		self.assertEquals(response.reason_phrase, 'OK')
		self.assertEquals(response.content, 'Операция перевода успешно завершена')

	def test_negative_pay(self):
		"""
		try to pay negative money count
		"""
		request = self.factory.post('/', self.post_data_negative_payment)
		error = False

		try:
			view = UserList.as_view()
			view(request)
		except AssertionError:
			error = True

		self.assertTrue(error)

	def test_validate_payment(self):
		"""
		validate user balance after the money transfer
		"""
		from_user = User.objects.get(user_id=2)
		from_user_balance = from_user.money_balance
		request = self.factory.post('/', self.post_data)
		view = UserList.as_view()
		view(request)
		from_user = User.objects.get(user_id=2)
		self.assertEquals(from_user.money_balance, from_user_balance - 100)

	def test_db_contains_users(self):
		"""
		check db is not empty (has users for money transfer)
		"""
		response = list(User.objects.all())
		self.assertTrue(len(response) > 0)
