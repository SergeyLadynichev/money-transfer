# -*- coding: utf-8 -*-

"""
User model. Users can receive and transfer money. Inn is not unique
"""

from django.db import models


class User(models.Model):

	user_id = models.AutoField(primary_key=True)
	first_name = models.CharField(max_length=200)
	second_name = models.CharField(max_length=200)
	middle_name = models.CharField(max_length=200)
	money_balance = models.DecimalField(max_digits=10, decimal_places=2)
	inn_code = models.IntegerField()

	def can_pay(self, money_to_pay):
		"""
		:param money_to_pay: decimal, amount of transfer
		:return: True if user has money, else False
		"""
		balance = self.money_balance - money_to_pay
		return True if balance > 0 else False
