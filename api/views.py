# -*- coding: utf-8 -*-

from decimal import Decimal

from django.db import transaction
from django.http import HttpResponse
from rest_framework import generics

from .models import User
from .serializers import UserSerializer


class UserList(generics.ListCreateAPIView):

	queryset = User.objects.all()
	serializer_class = UserSerializer

	@transaction.atomic
	def post(self, request, *args, **kwargs):
		"""
		logic for money transfers
		"""
		from_user, to_users, money_count = UserList.parse_post_data(request.data)

		if len(list(to_users)) == 0:
			return HttpResponse('В базе данных отсутствуют пользователь/пользователи'
			                    ' с указанным ИНН кодом. Так же пользователь'
			                    ' не может перевести деньги сам себе', status=500)

		if from_user.can_pay(money_count):

			average_transfer = money_count / len(to_users)

			for user in to_users:

				# cash receipts
				user.money_balance += average_transfer
				user.save()

			# write-off of money
			from_user.money_balance -= money_count
			from_user.save()

		else:
			return HttpResponse('У пользователя недостаточно'
								'денежных средств для осуществления операции', status=500)

		return HttpResponse('Операция перевода успешно завершена', status=200)

	@staticmethod
	def parse_post_data(data):
		"""
		:param data: form fields from client (Post request body)
		:return: users and transfer (decimal)
		"""

		# user_id of user who transfers
		from_user = User.objects.get(user_id=data['from_user'])

		# inn-code of beneficiary's accounts
		inn_codes = data['to_users'].strip().replace(" ", '').split(',')

		to_users = []

		# if at least one code is not valid, cancel the all transfer
		for inn_code in inn_codes:

			valid_users = list(User.objects.filter(inn_code=inn_code)\
				.exclude(user_id=from_user.user_id))

			if len(valid_users) == 0:
				to_users = []
				break
			else:
				to_users += valid_users

		# amount of transfer (can be shared between several users)
		money_count = Decimal(data['money_count'])

		assert money_count > 0

		return from_user, to_users, money_count
