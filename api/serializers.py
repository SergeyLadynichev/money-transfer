# -*- coding: utf-8 -*-

from rest_framework import serializers

from .models import User


class UserSerializer(serializers.ModelSerializer):
	"""
	Serializer for the user model. Returns the fields to the client.
	Using on GET requests
	"""
	class Meta:
		model = User
		field = ('user_id', 'first_name', 'second_name', 'middle_name',
		         'money_balance', 'inn_code')
